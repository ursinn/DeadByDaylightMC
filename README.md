# DeadByDaylightMC

<!--
[![Jenkins](https://img.shields.io/jenkins/build?jobUrl=https%3A%2F%2Fci.ursinn.dev%2Fjob%2Fursinn%2Fjob%2FDeadByDaylightMC&logo=jenkins&style=for-the-badge)](https://ci.ursinn.dev/job/ursinn/job/DeadByDaylightMC)
-->
[![GitHub last commit](https://img.shields.io/github/last-commit/ursinn/DeadByDaylightMC?logo=github&style=for-the-badge)](https://github.com/ursinn/DeadByDaylightMC/commits)
[![Code Climate maintainability](https://img.shields.io/codeclimate/maintainability/ursinn/DeadByDaylightMC?logo=codeclimate&style=for-the-badge)](https://codeclimate.com/github/ursinn/DeadByDaylightMC)
[![Snyk Vulnerabilities for GitHub Repo](https://img.shields.io/snyk/vulnerabilities/github/ursinn/DeadByDaylightMC?logo=snyk&style=for-the-badge)](https://snyk.io/test/github/ursinn/DeadByDaylightMC)
