package noahnok.dbdl.files.game.player.states;

import lombok.Getter;
import lombok.Setter;
import noahnok.dbdl.files.player.DPlayer;
import org.bukkit.scheduler.BukkitTask;

public class PlayerState {

    @Getter
    private final DPlayer player;
    // Ingame playable states
    @Getter
    @Setter
    private boolean bleeding;
    @Getter
    @Setter
    private boolean crawling;
    @Getter
    @Setter
    private boolean injured;
    @Getter
    @Setter
    private boolean hooked;
    @Getter
    @Setter
    private boolean carried;
    @Getter
    @Setter
    private boolean beingHealed;
    // Only applicable for the HUNTER/KILLER
    @Getter
    @Setter
    private boolean carrying;
    @Getter
    @Setter
    private boolean stunned;
    @Getter
    @Setter
    private boolean blind;
    @Getter
    @Setter
    private boolean fatigued;
    // Game end states
    @Getter
    @Setter
    private EndGameStates endGameState;
    @Getter
    @Setter
    private HookedStages hookedStage;
    @Getter
    @Setter
    private BukkitTask bleedingRunnable;

    public PlayerState(DPlayer player) {
        this.player = player;
        bleeding = false;
        crawling = false;
        injured = false;
        hooked = false;
        carried = false;
        endGameState = EndGameStates.NONE;
        hookedStage = HookedStages.NOT_HOOKED;
    }
}
