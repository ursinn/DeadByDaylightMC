package noahnok.dbdl.files.game;

import lombok.Getter;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class DGamemode {
    @Getter
    private final String id;

    @Getter
    private int hunters;
    @Getter
    private int hunted;
    @Getter
    private int maxgenerators;
    @Getter
    private int maxchests;
    @Getter
    private int maxhooks;

    // gameTime In seconds
    @Getter
    private int gameTime;
    @Getter
    private boolean allowPerks;
    @Getter
    private boolean allowItems;
    @Getter
    private boolean allowSacrifices;
    @Getter
    private boolean allowBleeding;
    @Getter
    private boolean allowStage3;
    @Getter
    private boolean allowStage2;
    @Getter
    private boolean instantSacrifice;
    @Getter
    private boolean useTrapdoor;

    private Set<ItemStack> disallowedItems = new HashSet<>();
    private Set<String> disallowedPerks = new HashSet<>();
    private Set<String> disallowedSacrifices = new HashSet<>();

    public DGamemode(String id, int hunters, int hunted, int maxgenerators, int maxchests, int maxhooks, int gameTime,
                     boolean allowPerks, boolean allowItems, boolean allowSacrifices, boolean allowBleeding,
                     boolean allowStage3, boolean allowStage2, boolean instantSacrifice, boolean useTrapdoor,
                     Set<ItemStack> disallowedItems, Set<String> disallowedPerks, Set<String> disallowedSacrifices) {
        this.id = id;
        this.hunters = hunters;
        this.hunted = hunted;
        this.maxgenerators = maxgenerators;
        this.maxchests = maxchests;
        this.maxhooks = maxhooks;
        this.gameTime = gameTime;
        this.allowPerks = allowPerks;
        this.allowItems = allowItems;
        this.allowSacrifices = allowSacrifices;
        this.allowBleeding = allowBleeding;
        this.allowStage3 = allowStage3;
        this.allowStage2 = allowStage2;
        this.instantSacrifice = instantSacrifice;
        this.useTrapdoor = useTrapdoor;
        this.disallowedItems = Collections.unmodifiableSet(disallowedItems);
        this.disallowedPerks = Collections.unmodifiableSet(disallowedPerks);
        this.disallowedSacrifices = Collections.unmodifiableSet(disallowedSacrifices);
    }

    public DGamemode(String id, int hunters, int hunted, int maxgenerators, int maxchests, int maxhooks, int gameTime,
                     boolean allowPerks, boolean allowItems, boolean allowSacrifices, boolean allowBleeding,
                     boolean allowStage3, boolean allowStage2, boolean instantSacrifice, boolean useTrapdoor) {
        this.id = id;
        this.hunters = hunters;
        this.hunted = hunted;
        this.maxgenerators = maxgenerators;
        this.maxchests = maxchests;
        this.maxhooks = maxhooks;
        this.gameTime = gameTime;
        this.allowPerks = allowPerks;
        this.allowItems = allowItems;
        this.allowSacrifices = allowSacrifices;
        this.allowBleeding = allowBleeding;
        this.allowStage3 = allowStage3;
        this.allowStage2 = allowStage2;
        this.instantSacrifice = instantSacrifice;
        this.useTrapdoor = useTrapdoor;
    }

    public DGamemode(String id) {
        this.id = id;
    }

    public Set<ItemStack> getDisallowedItems() {
        return Collections.unmodifiableSet(disallowedItems);
    }

    public Set<String> getDisallowedPerks() {
        return Collections.unmodifiableSet(disallowedPerks);
    }

    public Set<String> getDisallowedSacrifices() {
        return Collections.unmodifiableSet(disallowedSacrifices);
    }
}
