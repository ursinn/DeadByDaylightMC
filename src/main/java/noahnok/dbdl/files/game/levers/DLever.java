package noahnok.dbdl.files.game.levers;

import lombok.Getter;
import lombok.Setter;
import noahnok.dbdl.files.game.DGame;
import noahnok.dbdl.files.game.ExitGate;
import noahnok.dbdl.files.player.DPlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.material.Lever;

public class DLever {

    @Getter
    private final Location loc;
    @Getter
    private final Block block;
    private DGame game;
    @Getter
    private ExitGate gate;

    @Getter
    private int percentDone;
    @Getter
    @Setter
    private boolean finished;

    public DLever(Location loc, DGame game, ExitGate gate) {
        this.game = game;
        this.loc = loc;
        this.gate = gate;
        this.block = loc.getBlock();

        block.setType(Material.LEVER);

        BlockState blockState = block.getState();
        Lever lever = (Lever) blockState.getData();

        BlockFace[] blockFaces = {BlockFace.EAST, BlockFace.NORTH, BlockFace.WEST, BlockFace.SOUTH};
        for (BlockFace bf : blockFaces) {
            Block bloc = block.getRelative(bf);
            if (bloc.getType() == Material.IRON_BLOCK) {
                lever.setFacingDirection(bf.getOppositeFace());
                break;
            }
        }

        blockState.update();
    }

    public void kill() {
        this.game = null;
        this.gate = null;
    }

    public void increment(DPlayer p) {
        percentDone++;
        if (percentDone >= 100) {
            this.finished = true;
            BlockState blockState = block.getState();
            Lever lever = (Lever) blockState.getData();
            lever.setPowered(true);
            blockState.update();
            gate.openGate();
            game.announce("A gate has been opened!");
            game.setCanEscape(true);
            p.setGatesOpened(p.getGatesOpened() + 1);
            return;
        }
        p.addToScore((int) (10 * game.getMultiplier()));
    }
}
