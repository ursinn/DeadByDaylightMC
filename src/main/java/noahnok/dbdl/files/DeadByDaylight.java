package noahnok.dbdl.files;

import lombok.Getter;
import lombok.Setter;
import noahnok.dbdl.files.commands.*;
import noahnok.dbdl.files.game.*;
import noahnok.dbdl.files.game.events.MainEvents;
import noahnok.dbdl.files.game.player.states.PlayerStateManager;
import noahnok.dbdl.files.game.runnables.NoJump;
import noahnok.dbdl.files.general.events.GenericEvents;
import noahnok.dbdl.files.player.DPlayer;
import noahnok.dbdl.files.player.DPlayerManager;
import noahnok.dbdl.files.signs.SignEvents;
import noahnok.dbdl.files.signs.SignManager;
import noahnok.dbdl.files.utils.*;
import noahnok.dbdl.files.utils.editor.item.EditorEvents;
import noahnok.dbdl.files.utils.mysql.MySQL_Connect;
import noahnok.dbdl.files.utils.pagenation.PageEvent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.Scoreboard;

import java.util.Objects;
import java.util.UUID;

public class DeadByDaylight extends JavaPlugin {

    public Scoreboard sbrd;

    public String prefix = ChatColor.translateAlternateColorCodes('&', "&8[&7DBDL&8] &7");

    @Getter
    private DArenaManager arenaManager;

    @Getter
    private DGameManager gameManager;

    @Getter
    private DGamemodeManager gamemodeManager;

    @Getter
    private ArenaManagmentInvs arenaInvManager;

    @Getter
    private SetUpDefaults defaults;

    @Getter
    private ArenaEditor arenaEditor;

    @Getter
    private MatchMaking matchMaking;

    @Getter
    private SignManager signManager;

    @Getter
    private DPlayerManager dPlayerManager;

    @Getter
    private MySQL_Connect sqlManager;

    //As far as I know I cant inject these??

    @Getter
    @Setter
    private Config gamemodesConfig;

    @Getter
    @Setter
    private Config arenasConfig;

    @Getter
    @Setter
    private Config signConfig;

    @Getter
    @Setter
    private Config messagesConfig;

    private GenericEvents ge;

    private MainEvents me;

    private SignEvents se;

    private EditorEvents ee;

    private InventoryEvents ie;

    private ReadyConfigs readyConfigs;

    private MainCommands mainCommands;

    private ArenaCommands arenaCommands;

    private JoinGameCommand joinGameCommand;

    private LeaveCommand leaveCommand;

    @Getter
    private Toggles toggles;

    @Getter
    private MessageUtils messageUtils;

    @Getter
    private PlayerStateManager playerStateManager;

    private BukkitTask noJump;

    private StatsCommand statsCommand;

    public JoinGameCommand getJoinGameCommand() {
        return joinGameCommand;
    }

    @Override
    public void onEnable() {
        setAccess();
        saveDefaultConfig();

        this.getCommand("dbdl").setExecutor(mainCommands);

        this.getCommand("stats").setExecutor(statsCommand);

        this.getCommand("arena").setExecutor(arenaCommands);
        this.getCommand("join").setExecutor(joinGameCommand);
        this.getCommand("leave").setExecutor(leaveCommand);
        this.getServer().getPluginManager().registerEvents(ie, this);
        this.getServer().getPluginManager().registerEvents(ee, this);
        this.getServer().getPluginManager().registerEvents(se, this);
        this.getServer().getPluginManager().registerEvents(me, this);
        this.getServer().getPluginManager().registerEvents(ge, this);

        readyConfigs.createConfigs();

        toggles.setUpToggles();

        sqlManager.initConnection();

        sbrd = this.getServer().getScoreboardManager().getMainScoreboard();
        defaults.initialiseBasics();
        arenaManager.loadArenasFromFile();
        signManager.loadSignsFromFile();

        for (Player player : this.getServer().getOnlinePlayers()) {
            dPlayerManager.loadDPlayer(player.getUniqueId());
        }

        this.getServer().getPluginManager().registerEvents(new PageEvent(), this);

        noJump = new NoJump(this).runTaskTimer(this, 0, (20 * 8));
    }

    @Override
    public void onDisable() {
        noJump.cancel();

        for (UUID id : arenaEditor.editing.keySet()) {
            arenaEditor.stopEditing(getServer().getPlayer(id));
            getServer().getPlayer(id).sendMessage("You were forcefully removed from editing due to a reload!");
        }

        // Take players out of running games!
        for (DGame game : gameManager.getGames()) {
            for (DPlayer dplayer : game.getPlayers()) {

                if (Objects.requireNonNull(dplayer).getCurrentGame() != null) {
                    getGameManager().removePlayerFromGame(dplayer.getPlayer(), dplayer.getCurrentGame());
                }
            }
        }

        getArenaManager().saveArenasToFile();
        signManager.saveSignsToFile();
        dPlayerManager.savePlayerData();

        sqlManager.closeConnection();
    }

    private void setAccess() {
        arenaManager = new DArenaManager(this);
        gameManager = new DGameManager(this);
        gamemodeManager = new DGamemodeManager(this);
        arenaInvManager = new ArenaManagmentInvs(this);
        defaults = new SetUpDefaults(this);
        arenaEditor = new ArenaEditor(this);
        matchMaking = new MatchMaking(this);
        signManager = new SignManager(this);
        dPlayerManager = new DPlayerManager(this);
        sqlManager = new MySQL_Connect(this);
        ge = new GenericEvents(this);
        me = new MainEvents(this);
        se = new SignEvents(this);
        ee = new EditorEvents(this);
        ie = new InventoryEvents();
        readyConfigs = new ReadyConfigs(this);
        mainCommands = new MainCommands(this);
        arenaCommands = new ArenaCommands(this);
        joinGameCommand = new JoinGameCommand(this);
        leaveCommand = new LeaveCommand(this);
        toggles = new Toggles(this);
        messageUtils = new MessageUtils(this);
        playerStateManager = new PlayerStateManager(this);
        statsCommand = new StatsCommand(this);
    }

    //public Config getMessagesConfig() {
    //return messagesConfig;
    //}
}
