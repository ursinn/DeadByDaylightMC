package noahnok.dbdl.files.player;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import noahnok.dbdl.files.DeadByDaylight;
import noahnok.dbdl.files.game.DGame;
import noahnok.dbdl.files.game.player.states.PlayerState;
import noahnok.dbdl.files.game.runnables.SpectatingRunnable;
import noahnok.dbdl.files.utils.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DPlayer {

    private final DeadByDaylight main;

    @Getter
    private UUID id;
    @Getter
    private String name;
    private Config dataFile;
    @Getter
    @Setter
    private PlayerStatus status = PlayerStatus.OUT_OF_GAME;

    @Getter
    private PlayerState playerState;

    @Getter
    @Setter
    private boolean spectating;

    @Getter
    private DPlayer spectate;

    @Getter
    private BukkitTask spectatingRunnable;

    @Getter
    private int gameScore;

    @Getter
    @Setter
    private DGame currentGame;

    //Config values to be loaded
    @Getter
    @Setter
    private int bloodPoints;
    @Getter
    @Setter
    private int score;

    @Getter
    @Setter
    private int escapes;
    @Getter
    @Setter
    private int deaths;
    @Getter
    @Setter
    private int timesSacrificed;
    @Getter
    @Setter
    private int generatorsFixed;
    @Getter
    @Setter
    private int generatorsMessedup;
    @Getter
    @Setter
    private int timesHooked;
    @Getter
    @Setter
    private int hookEscapes;
    @Getter
    @Setter
    private int hookPulloff;
    @Getter
    @Setter
    private int gatesOpened;
    @Getter
    @Setter
    private int hooksBroken;
    @Getter
    @Setter
    private int heals;

    @Getter
    @Setter
    private int timesSacrificing;
    @Getter
    @Setter
    private int successfulSacrifices;
    @Getter
    @Setter
    private int kills;
    @Getter
    @Setter
    private int losses;
    @Getter
    @Setter
    private int survivorPickups;
    @Getter
    @Setter
    private int survivorPickupEscapes;
    @Getter
    @Setter
    private int playerEscapes;
    @Getter
    @Setter
    private int wins;

    public DPlayer(UUID id, Config dataFile, DeadByDaylight main) {
        this.id = id;
        this.dataFile = dataFile;
        this.name = Bukkit.getServer().getPlayer(id).getName();
        this.main = main;
    }

    public Map<String, Integer> returnGenericData() {
        Map<String, Integer> values = new HashMap<>();
        values.put("bloodPoints", bloodPoints);

        return values;
    }

    public void resetPlayerState() {
        if (playerState != null) {
            main.getPlayerStateManager().survivorHealed(this, true);
            if (playerState.getBleedingRunnable() != null) {
                playerState.getBleedingRunnable().cancel();
                playerState.setBleedingRunnable(null);
            }
        }

        playerState = new PlayerState(this);
    }

    public void stopSpectating() {
        if (this.getSpectatingRunnable() != null) {
            this.getSpectatingRunnable().cancel();
            this.spectatingRunnable = null;
        }
    }

    public void startSpectating(DGame game) {
        Player player = this.getPlayer();
        player.setGameMode(GameMode.SPECTATOR);
        resetPlayerState();

        for (DPlayer dplayer : game.getHunted()) {
            if (dplayer != null && !dplayer.isDead() && !dplayer.isHunter()) {
                player.setSpectatorTarget(dplayer.getPlayer());
                this.setSpectating(true);
                spectate = dplayer;
                this.spectatingRunnable = new SpectatingRunnable(dplayer.getName(), this).runTaskTimer(main, 0, 80);
                return;
            }
        }

    }

    public void spectateNext(DPlayer player) {
        if (this.getPlayer().getGameMode() != GameMode.SPECTATOR) {
            this.getPlayer().setGameMode(GameMode.SPECTATOR);
        }

        if (!this.isSpectating()) {
            this.setSpectating(true);
        }

        this.getPlayer().setSpectatorTarget(player.getPlayer());
        spectate = player;
        spectatingRunnable.cancel();
        this.spectatingRunnable = new SpectatingRunnable(player.getName(), this).runTaskTimer(main, 0, 80);
    }

    public boolean isPlayerASurvivorAndAlive() {
        return status.isAlive();
    }

    public boolean isHunter() {
        switch (this.status) {
            case HUNTER:
                return true;

            case CARRYING:
                return true;

            default:
                return false;
        }
    }

    public boolean isDead() {
        switch (this.status) {
            case DEAD:
                return true;

            case ESCAPED:
                return true;

            case SACRIFICED:
                return true;

            default:
                return false;
        }
    }

    public Player getPlayer() {
        return Bukkit.getServer().getPlayer(id);
    }

    public void addToScore(int amount) {
        gameScore += amount;
    }

    public void addToStaticScore(int value) {
        score += value;
    }

    public void clearGameScore() {
        gameScore = 0;
    }

    public FileConfiguration getDataFile() {
        return dataFile.getConfig();
    }

    public Config grabConfig() {
        return dataFile;
    }

    public void kill() {
        id = null;
        name = null;
        dataFile = null;
    }

    public Map<String, Integer> returnHuntedData() {
        Map<String, Integer> values = new HashMap<>();
        values.put("escapes", escapes);
        values.put("deaths", deaths);
        values.put("timesSacrificed", timesSacrificed);
        values.put("generatorsFixed", generatorsFixed);
        values.put("generatorsMessedup", generatorsMessedup);
        values.put("timesHooked", timesHooked);
        values.put("hookEscapes", hookEscapes);
        values.put("hookPulloff", hookPulloff);
        values.put("gatesOpened", gatesOpened);
        values.put("hooksBroken", hooksBroken);
        values.put("heals", heals);

        return values;
    }

    public Map<String, Integer> returnHunterData() {
        Map<String, Integer> values = new HashMap<>();
        values.put("timesSacrificing", timesSacrificing);
        values.put("successfulSacrifices", successfulSacrifices);
        values.put("kills", kills);
        values.put("losses", losses);
        values.put("survivorPickups", survivorPickups);
        values.put("survivorPickupEscapes", survivorPickupEscapes);
        values.put("playerEscapes", playerEscapes);

        return values;
    }

    //Messaging utils for player

    public void sendAB(String message) {
        this.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR,
                TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', message)));
    }
}
